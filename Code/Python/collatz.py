# -*- coding: utf-8 -*-
"""
Created on Thu Nov  3 03:46:46 2016

@author: Admin
"""


import random

def collatz(number):
        if (number % 2) == 0:
            print(str(number) + ' // 2')
            return(number // 2)
        else:
            print('3 * ' + str(number) + ' + 1')
            return(3 * number + 1)
        
for i in range(1,20):
    print('The value of i is ' + str(i))
    print('collatz returned the following result ' + str(collatz(i)))