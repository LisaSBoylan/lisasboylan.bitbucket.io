# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 23:36:18 2016

@author: Admin
"""

theBoard = {'1':'_', '2':'_', '3':'_', '4':'_', '5':'_', '6':'_', '7':'_', '8':'_', '9':'_'}
            
       
def oMove(position):
        theBoard[position] = 'O'

def xMove(position):
        theBoard[position] = 'X' 
      
def printBoard():
    print('__' + theBoard['1'] + '__|' + '__' + theBoard['2'] + '__|' + '__' + theBoard['3'] + '__')
    print('__' + theBoard['4'] + '__|' + '__' + theBoard['5'] + '__|' + '__' + theBoard['6'] + '__')
    print('__' + theBoard['7'] + '__|' + '__' + theBoard['8'] + '__|' + '__' + theBoard['9'] + '__')
    
def isTTT(token, sequence):
    count = 0
    for i in sequence:
        if theBoard[str(i)] == token:
            count += 1
            if count == 3:
                return(True) 
        else:
            return(False)   
    
def checkTTT(token):    
    
    # Horizontal TTT
    for i in range(1,8,3):
        [a,b,c] = range(i,i+3)
        if isTTT(token,[a,b,c]):
            return(True)
                  
    # Vertical TTT
    for i in range(1,4):
        [a,b,c] = range(i,i+7,3)
        if isTTT(token,[a,b,c]):
            return(True)
            
    # Diagonal TTT
    if theBoard['5'] == token:
        if theBoard['1'] == token:
            if theBoard['9'] == token:
                return True
        elif theBoard['3'] == token:
            if theBoard['7'] == token:
                return True
    return False

    
# Main Driver
count = 1
token = 'O'

print('O\'s will go first')
print('Enter a move (number 1 thru 9) or q to quit')

while '_' in theBoard.values():
    printBoard()
    print('choice: ', end=' ')
    move = input()
    print('\n')
    
    if move == 'q':
        print('Sorry you are unble to finish. Come again soon')
        break
    elif (move not in ['1','2','3','4','5','6','7','8','9']) or (theBoard[move] != '_' ):
        print('I\'m am sorry, ' + str(move) + ' is not a valid move.')
        print('Choose a number between 1 and 9, but not: ' + str(move))
        continue
    else: # Valid Move
       count += 1
       if count % 2 == 0:
           token = 'O'
           oMove(move)
       else:
           token = 'X'
           xMove(move)

    # Check For Tic-Tac-Toe    
    if checkTTT(token):
        printBoard()
        print('Tick-Tac-Toe!   Congradulations player ' + token)
        break
    elif (count==10):
        printBoard()
        print('It\'s a tie! Great Game...')
        break
    
    
    
'''
Could also use:
turn = 'X'
while loop
      # Valid Move
      theBoard[move] = turn
      if turn == 'X':
            turn = 'O'
        else:
            turn = 'X'
'''